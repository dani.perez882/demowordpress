<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'arrocera');

/** MySQL database username */
define('DB_USER', 'arrocera');

/** MySQL database password */
define('DB_PASSWORD', 'arrocera');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', 'n1dn67$s]5~F#cQ&5t9!5(^tS:RvZ/=Wr;LmYC`F%V@=Q;#,G&nomlBrdOyibyAp' );
define( 'SECURE_AUTH_KEY', '7gGU&xmUS 0M4Mf4?by#CeM333UcJI-)se&&z0tq:E>D:nSy1w=6EVu]**05Z<1t' );
define( 'LOGGED_IN_KEY', 'a1owY~[=:*vt<X1u}(fP(gEzY52=jIhK4l5_eX^J PU9kkN&jS8w)/322Ll=>i%r' );
define( 'NONCE_KEY', 'jT3WR1/|3>!SJm.Bgtt[HHe6f_VG^4J<{RW!EMx~Dc70Dne;>nJo?D#QSAiypmI&' );
define( 'AUTH_SALT', 'Gm~9>Rc!PY|{Y/1a=eJ[k[RuxFLks&3JN7`C/jYCQHhfrSjhEMx-zog)AYw9Zl|v' );
define( 'SECURE_AUTH_SALT', '6wUzZ/jX==3owsKQ&oCF,X[jv/x<uGZ24Q^j?gb_I_]2(Sr$D.YeVQ;Oj]fOaKb;' );
define( 'LOGGED_IN_SALT', 'H]huiCoFeG:^4yewxm=wRUzWf}R)SQ(O ^YJ&J}<-`I-&(4lmXT1~)TNv,t%`7@>' );
define( 'NONCE_SALT', '~Nz=*d-~N#*DvvUX1=`p8|2](T<d#ucevy[uX3bGI!}:<$kXNyKhox5aVl}jd]=f' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'mt_';


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
